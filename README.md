### Imi-Auth
安装：composer require phpben/imi-auth
<br><br>
逻辑，表按照tp-auth进行开发<br>
用法与laravel-auth基本同理，主要使用注解验证，手动验证马上回来<br>
可以实现Phpben\Imi\Auth\Contract\TokenContract开发自己的token鉴权方式，默认有jwt与session<br>
可以实现Phpben\Imi\Auth\Contract\HashContract开发自己的密码验证方式，默认有md5和md5salt<br>
<br><br>
该组件设计为了不单单只让auth适用于后台，前台/api/代理模块等等都可以使用该组件，添加不同配置即可，如无需规则权限验证，关闭check即可<br>
后台管理员获取当前组别，规则等可以看 Phpben\Imi\Auth\Auth, 静态调用 方法与tpauth无异，不一一讲解了<br>
AuthManager 是自动调用jwt/session/自定义类的管理类 可以使用guard方法调用非配置定义的类<br>
Auth 是管理员/权限/组的快捷方法，里面内置获取用户所有组，树状组，所属自己的下级管理员等方法，具体可以使用ide或看文件注释了解
<br><br>
需要在config/beans.php配置添加auth相关配置<br>
jwt需要安装imi-jwt组件<br>
缓存名称需按[imi缓存一节](https://doc.imiphp.com/v2.0/components/cache/index.html)配置<br>
表可以自行创建自己喜欢的表格式，也可以直接复制tp-auth的表，在配置中设置字段名称即可，当然表的id是必须的<br>
auth_rule中验证的规则不在采用路由地址验证，内容为类名::方法，例：ImiApp\ApiServer\Backend\Controller\TestController::login<br>
```
    'AuthConfig' => [
        // 是否开启 
        'status' => true,
        // 配置名称-默认配置
        'default' => [
            // 鉴权方式 Session稍后支持
            'auth' => \Phpben\Imi\Auth\Jwt::class,
            // IMI-缓存配置名称
            'cache' => "redis",
            // 一个账号只能一个人登陆
            'unique' => true,
            // 是否判断权限规则
            'check' => true,
            // JWT配置 结合IMI-JWT组件使用
            'jwt' => [
                // IMI-JWT名称
                'name' => 'default',
                // JWT Header名称
                'header_name' => 'Authorization',
                // JWT 前缀
                'prefix' => 'Bearer ',
            ],
            // 错误配置
            'error' => [
                // 错误Code值
                'code' => 401,
                // 错误http状态码
                'status_code' => 401,
                // 错误消息
                'message' => 'auth error',
            ],
            // 模型配置
            'model' => [
                // 用户表模型
                'user' => \ImiApp\ApiServer\Backend\Model\SoAdmin::class,
                // 登陆日志表模型
                'login_log' => \ImiApp\ApiServer\Backend\Model\SoAdminLoginLog::class,
                // 操作日志表模型
                'operate_log' => \ImiApp\ApiServer\Backend\Model\SoAdminOperateLog::class,
                // 权限组模型 如果需要验证组和规则 auth_group模型必须引入imi的树状模型trait并定义相关代码，具体可以看                 
                // https://doc.imiphp.com/v2.0/components/orm/TreeModel.html
                'auth_group' => \ImiApp\ApiServer\Backend\Model\SoAuthGroup::class,
                // 权限组关系表模型
                'auth_group_access' => \ImiApp\ApiServer\Backend\Model\SoAuthGroupAccess::class,
                // 权限规则表模型
                'auth_rule' => \ImiApp\ApiServer\Backend\Model\SoAuthRule::class,
            ],
            // 设置
            'settings' => [
                // 登陆日志
                'login_log' => true,
                // 操作日志
                'operate_log' => true,
                // Hash密码类
                'hash' => \Phpben\Imi\Auth\Hasher\Md5Salt::class,
                // Hash类配置
                'hash_option' => [

                ],
                // 用户表字段
                'user_keys' => [
                    // username唯一用户名
                    'username' => 'username',
                    // password 密码
                    'password' => 'password',
                    // salt 密码盐，非必需
                    'salt' => 'salt',
                    // token 存入字段
                    'token' => 'token'
                ],
                // 规则表字段
                'auth_rule_keys' => [
                    // 规则表字段 内容例： ImiApp\ApiServer\Controller\TestController::login
                    'rule' => 'rule',
                    // 规则所属分组字段
                    'pid' => 'pid',
                ],
                'auth_group_keys' => [
                    // 规则Ids字段 内容例：1,3,5,7,9 支持*
                    'ids' => 'rules',
                    // 开关字段 1/0判断
                    'status' => 'status',
                    // 所属user字段id
                    'pid' => 'pid'
                ],
                'auth_group_access_keys' => [
                    // 用户id
                    'user_id' => 'uid',
                    // auth_group组id
                    'group_id' => 'gid'
                ]
            ]
        ],
        // 后端
        'backend' => [
            // 鉴权方式 Session稍后支持
            'auth' => \Phpben\Imi\Auth\Jwt::class,
            // IMI-缓存配置名称
            'cache' => "redis",
            // 一个账号只能一个人登陆
            'unique' => true,
            // 是否判断权限规则
            'check' => true,
            // JWT配置 结合IMI-JWT组件使用
            'jwt' => [
                // IMI-JWT名称
                'name' => 'default',
                // JWT Header名称
                'header_name' => 'Authorization',
                // JWT 前缀
                'prefix' => 'Bearer ',
            ],
            // 错误配置
            'error' => [
                // 错误Code值
                'code' => 401,
                // 错误http状态码
                'status_code' => 401,
                // 错误消息
                'message' => 'auth error',
            ],
            // 模型配置
            'model' => [
                // 用户表模型
                'user' => \ImiApp\ApiServer\Backend\Model\SoAdmin::class,
                // 登陆日志表模型
                'login_log' => \ImiApp\ApiServer\Backend\Model\SoAdminLoginLog::class,
                // 操作日志表模型
                'operate_log' => \ImiApp\ApiServer\Backend\Model\SoAdminOperateLog::class,
                // 权限组模型
                'auth_group' => \ImiApp\ApiServer\Backend\Model\SoAuthGroup::class,
                // 权限组关系表模型
                'auth_group_access' => \ImiApp\ApiServer\Backend\Model\SoAuthGroupAccess::class,
                // 权限规则表模型
                'auth_rule' => \ImiApp\ApiServer\Backend\Model\SoAuthRule::class,
            ],
            // 设置
            'settings' => [
                // 登陆日志
                'login_log' => true,
                // 操作日志
                'operate_log' => true,
                // Hash密码类
                'hash' => \Phpben\Imi\Auth\Hasher\Md5Salt::class,
                // Hash类配置
                'hash_option' => [

                ],
                // 用户表字段
                'user_keys' => [
                    // username唯一用户名
                    'username' => 'username',
                    // password 密码
                    'password' => 'password',
                    // salt 密码盐，非必需
                    'salt' => 'salt',
                    // token 存入字段
                    'token' => 'token'
                ],
                // 规则表字段
                'auth_rule_keys' => [
                    // 规则表字段 内容例： ImiApp\ApiServer\Controller\TestController::login
                    'rule' => 'rule',
                    // 规则所属分组字段
                    'pid' => 'pid',
                ],
                'auth_group_keys' => [
                    // 规则Ids字段 内容例：1,3,5,7,9 支持*
                    'ids' => 'rules',
                    // 开关字段 1/0判断
                    'status' => 'status',
                    // 所属user字段id
                    'pid' => 'pid'
                ],
                'auth_group_access_keys' => [
                    // 用户id
                    'user_id' => 'uid',
                    // auth_group组id
                    'group_id' => 'gid'
                ]
            ]
        ],
    ],
```
注解使用方式（注解只能设置在类上）：<br>
注解：@Auth<br>
参数：<br>
    name = 对应beans配置中的键名<br>
    nologin = 当前类不需要验证登陆的方法，直接填方法名称即可，数组格式，也可以填入字符串*代表所有方法都不需要验证登陆<br>
    nocheck = 当前类不需要验证规则的方法，直接填方法名称即可，数组格式，也可以填入字符串*代表所有方法都不需要验证规则<br>
如果配置中check选项等于false，那么就相当于所有用了当前配置的auth注解对应的类都不会检测验证规则，相当于自动把所有auth注解都加了一个nocheck=“*”<br>

```
// 注解验证
// 示例：@Auth 什么参数都没有则使用default配置
<?php

declare(strict_types=1);

namespace ImiApp\ApiServer\Backend\Controller;

use Imi\Server\Http\Controller\HttpController;
use Imi\Server\Http\Route\Annotation\Action;
use Imi\Server\Http\Route\Annotation\Controller;
use Imi\Server\Http\Route\Annotation\Route;
use Phpben\Imi\Auth\Annotation\Auth;
use Phpben\Imi\Validate\Annotation\Validate;

/**
 * @Auth(name="backend",nologin={"login"},nocheck={"index"})
 * @Controller("/test/")
 */
class TestController extends HttpController
{
    /**
     * @Action
     * @Route(url="login",method="POST")
     * @Validate
     */
    public function login($data)
    {
        // 使用imi-auth进行登陆验证
        $auth = new \Phpben\Imi\Auth\AuthManager($name = '配置名称');
        if($user = $auth->login($username,$password)){
            var_dump($user);//登陆成功
        }
        // 或者你也可以使用imi注入成员变量
        if($user = $this->auth->guard($name='变量名称')->login($username,$password)){
            var_dump($user);//登陆成功
        }
        else
        {
            //账号或者密码错误
        }
        // 登陆完毕
        return ‘无需登陆’;
    }

    /**
     * @Action
     * @Route(url="index")
     * @Validate
     */
    public function index()
    {
        // 登陆成功后可以使用imi-auth获取当前用户信息

        $auth = new \Phpben\Imi\Auth\AuthManager($name = '配置名称');
        var_dump($auth->user());//用户信息模型对象
        // 或者你也可以使用imi注入成员变量
        var_dump($this->auth->guard(配置名称)->user();//用户信息模型对象
        // 或者你使用了默认配置
        var_dump($this->auth->user();//用户信息模型对象
        // 
        return '需要登陆,无须验证权限规则';
    }


    

    /**
     * @Action
     * @Route(url="api")
     * @Validate
     */
    public function api($user)
    {
        $auth = new \Phpben\Imi\Auth\AuthManager($name = '配置名称');
        $auth->auth()->getRuleList(); // 支持auth方法调用Auth类 


        var_dump($user); // 使用了auth注解的类方法都会自动注入一个user变量到方法中，你也可以直接接收user变量获取用户信息
        return '需要登陆,并且需要验证权限规则，因为nocheck中不包含当前方法';
    }

   

}

```
