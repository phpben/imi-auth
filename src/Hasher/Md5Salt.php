<?php

declare(strict_types=1);

namespace Phpben\Imi\Auth\Hasher;

use Phpben\Imi\Auth\Contract\HashContract;

class Md5Salt implements HashContract
{
    /**
     * 默认salt
     * @var string
     */
    private string $default_salt = 'QWP{L":DS<?>KJWO!U)(IEPO!@Klmw';

    /**
     * 检查
     * @param string $value
     * @param string $hash
     * @param array $option
     * @return bool
     */
    public function check(string $value, string $hash, array $option): bool
    {
        return $this->create($value, $option) == $hash;
    }

    /**
     * 创建
     * @param string $value
     * @param array $option
     * @return string
     */
    public function create(string $value, array $option): string
    {
        return md5(md5($value) . ($option['salt'] ?? $this->default_salt));
    }
}
