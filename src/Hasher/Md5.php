<?php

declare(strict_types=1);

namespace Phpben\Imi\Auth\Hasher;

use Phpben\Imi\Auth\Contract\HashContract;

class Md5 implements HashContract
{
    /**
     * 检查
     * @param string $value
     * @param string $hash
     * @param array $option
     * @return bool
     */
    public function check(string $value, string $hash, array $option): bool
    {
        return md5($value) == $hash;
    }

    /**
     * 创建
     * @param string $value
     * @param array $option
     * @return string
     */
    public function create(string $value, array $option): string
    {
        return md5($value);
    }
}
