<?php

namespace Phpben\Imi\Auth\Listener;

use Imi\Aop\AopManager;
use Imi\Bean\Annotation\AnnotationManager;
use Imi\Event\EventParam;
use Imi\Event\IEventListener;
use Imi\Bean\Annotation\Listener;
use Imi\Util\DelayClassCallable;
use Imi\Util\Imi;
use Phpben\Imi\Auth\Annotation\Auth;
use Phpben\Imi\Auth\Aop\AuthAop;

/**
 * IMI暂不支持Aop切入类 查找类所有方法遍历切入
 * @Listener(eventName="IMI.LOAD_RUNTIME", priority=19940291)
 */
class AuthListener implements IEventListener
{
    /**
     * 切入所有使用Auth注解的类下的每个方法
     * @param EventParam $e
     * @return void
     */
    public function handle(EventParam $e): void
    {
        $authList = AnnotationManager::getAnnotationPoints(Auth::class);
        foreach ($authList as $auth) {
            $class = $auth->getClass();
            $params = $auth->getAnnotation()->toArray();
            $configname = $params['name'];
            $nologin = $params['nologin'];
            $nocheck = $params['nocheck'];
            $paramsArr = [$configname, $nologin, $nocheck];
            if ($nologin !== '*') {
                $file = Imi::getNamespacePath($class);
                $fileContent = file(str_contains($file, '.php') ? $file : $file . '.php');
                $methods = [];
                foreach ($fileContent as $line) preg_match('~public function ([_A-Za-z0-9]+)~', $line, $regs) && $methods[] = $regs[1];
                $methods = array_diff($methods, (array)$nologin);
                //$methods = array_diff(get_public_class_methods($class), (array)$nologin); get_public_class_methods这个玩应影响热更新 服了
                foreach ($methods as $method) {
                    $callback = new DelayClassCallable(AuthAop::class, 'parseAuth', $paramsArr);
                    AopManager::addAround($class, $method, $callback, 999, [
                        'deny' => [],
                        'extra' => $params
                    ]);
                }
            }
        }
    }
}