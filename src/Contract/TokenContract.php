<?php

declare(strict_types=1);

namespace Phpben\Imi\Auth\Contract;

interface TokenContract
{
    /**
     * 解析
     * @param string|null $token
     * @param array $option
     * @return mixed
     */
    public function pasreToken(?string $token = null, array $option = []);

    /**
     * 获取
     * @param string|array $data
     * @param array $option
     * @return string
     */
    public function getToken($data, array $option = []): string;

    /**
     * 登陆
     * @param string $username
     * @param string $password
     * @param array $option
     * @return bool
     */
    public function login(string $username, string $password, array $option = []);

    /**
     * 退出登陆
     * @param string|null $token
     * @return bool
     */
    public function logout(?string $token = null): bool;

    /**
     * 是否登陆
     * @param string|null $token
     * @return bool
     */
    public function isLogin(?string $token = null): bool;

    /**
     * 用户信息
     * @return mixed
     */
    public function user(?string $token = null);
}
