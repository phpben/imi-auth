<?php

declare(strict_types=1);

namespace Phpben\Imi\Auth\Contract;

interface HashContract
{
    /**
     * 检查
     * @param string $value
     * @param string $hash
     * @param array $option
     * @return bool
     */
    public function check(string $value, string $hash, array $option): bool;

    /**
     * 创建
     * @param string $value
     * @param array $option
     * @return string
     */
    public function create(string $value, array $option): string;
}
