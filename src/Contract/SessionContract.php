<?php

declare(strict_types=1);

namespace Phpben\Imi\Auth\Contract;

interface SessionContract
{
    /**
     * 登陆
     * @param $user
     * @return bool
     */
    public function login($user): bool;

    /**
     * 退出登陆
     * @return bool
     */
    public function logout(): bool;

    /**
     * 是否登陆
     * @return bool
     */
    public function isLogin(): bool;

    /**
     * 用户信息
     * @return mixed
     */
    public function user();
}
