<?php

if (!function_exists('ip')) {
    function ip()
    {
        $request = \Imi\RequestContext::get('request');
        return $request->getHeaderLine('x-real-ip') ?: (string)$request->getClientAddress();;
    }
}

if (!function_exists('get_public_class_methods')) {
    /**
     * 获取类下所有公有方法
     * @param string $class 类名
     * @return array
     */
    function get_public_class_methods(string $class): array
    {
        $methods = get_class_methods($class);
        if ($parent_class = get_parent_class($class)) {
            $parent = get_class_methods($parent_class);
            $data = array_diff($methods, $parent);
        } else {
            $data = $methods;
        }
        return $data;
    }
}