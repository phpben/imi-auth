<?php

namespace Phpben\Imi\Auth\Annotation;

use Imi\Bean\Annotation\Base;
use Imi\Bean\Annotation\Parser;

/**
 * Auth注解
 * @Annotation
 * @Target("CLASS")
 * @Parser("\Imi\Bean\Parser\NullParser")
 *
 * @property null|string $name 配置名称
 * @property string|array $nologin 不需要登陆的方法
 * @property string|array $nocheck 不需要鉴权的方法
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Auth extends Base
{
    /**
     * 默认参数名
     * @var string|null
     */
    protected ?string $defaultFieldName = 'name';

    /**
     * @param array|null $__data
     * @param string|null $name
     * @param string|array $nologin
     * @param string|array $nocheck
     */
    public function __construct(?array $__data = null, ?string $name = null, $nologin = null, $nocheck = null)
    {
        is_null($nocheck) && $nocheck = [];
        is_null($nologin) && $nologin = [];
        parent::__construct(...\func_get_args());
    }
}